const fetch = require('node-fetch');

const createUser = async () => {
    const userData = {
        username: 'Дима Тестер',
        email: 'dima3@example.com',
        password: '1', // В реальном приложении рекомендуется использовать безопасное хэширование пароля
        role_id: 1, // ID роли пользователя

        birth_date: '1990-01-01' // Дата рождения пользователя
    };

    try {
        const response = await fetch('http://localhost:3000/user', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(userData)
        });

        if (!response.ok) {
            throw new Error('Failed to create user');
        }

        const newUser = await response.json();
        console.log('New user created:', newUser);
    } catch (error) {
        console.error('Error creating user:', error.message);
    }
};


createUser();