const fp = require('fastify-plugin');

module.exports = async function (fastify, opts) {
    // Регистрация вашего PositionsDAO
    fastify.register(require('../../services/positionsDAO'));

    // Маршруты для управления должностями

    // Создание новой должности
    fastify.post('/', async (request, reply) => {
        try {
            const { position_name } = request.body;
            const newPosition = await fastify.positionsDAO.createPosition(position_name);
            reply.code(201).send(newPosition);
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    // Маршрут для получения всех должностей
    fastify.get('/', async (request, reply) => {
        try {
            const positions = await fastify.positionsDAO.getAllPositions();
            return positions;
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    // Получение должности по её ID
    fastify.get('/:position_id', async (request, reply) => {
        try {
            const { position_id } = request.params;
            const position = await fastify.positionsDAO.getPositionById(position_id);
            if (!position) {
                reply.code(404).send({ error: 'Position not found' });
            }
            return position;
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    // Обновление существующей должности
    fastify.put('/:position_id', async (request, reply) => {
        try {
            const { position_id } = request.params;
            const { position_name} = request.body;
            const updatedPosition = await fastify.positionsDAO.updatePosition(position_id, position_name);
            return updatedPosition;
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    // Удаление должности
    fastify.delete('/:position_id', async (request, reply) => {
        try {
            const { position_id } = request.params;
            await fastify.positionsDAO.deletePosition(position_id);
            reply.status(204).send(); // Отправляем ответ без содержимого
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });
};
