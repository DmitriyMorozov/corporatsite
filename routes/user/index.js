const bcrypt = require('bcrypt');
const usersDAOPlugin = require('../../services/usersDAO');

module.exports = async function (fastify, opts) {
    // Регистрация нового DAO
    fastify.register(usersDAOPlugin);
  
    // Защищаем маршруты с помощью аутентификации
    // fastify.addHook('preHandler', fastify.authenticate);  

    // Маршруты для управления пользователями
    fastify.get('/', async (request, reply) => {
        const users = await fastify.userDAO.getAllUsers();
        return users;
    });

    fastify.get('/:user_id', async (request, reply) => {
        const { user_id } = request.params;
        const user = await fastify.userDAO.getUserById(user_id);
        return user;
    });

    fastify.post('/', async (request, reply) => {
        const { username, email, password, role_id, birth_date, avatar, position_id } = request.body;
        console.log('Received request body:', request.body);
        
        const newUser = await fastify.userDAO.createUser(username, email, password, role_id, birth_date, avatar, position_id);
        reply.code(201).send(newUser);
    });
    

    fastify.put('/:user_id', async (request, reply) => {
        const { user_id } = request.params;
        const { username, email, password, role_id, birth_date, avatar, position_id } = request.body;
        
        const updatedUser = await fastify.userDAO.updateUser(user_id, username, email, password, role_id, birth_date, avatar, position_id);
        return updatedUser;
    });

  
    fastify.delete('/:user_id', async (request, reply) => {
        try {
            const { user_id } = request.params;
            await fastify.userDAO.deleteUser(user_id);
            reply.code(204).send(); 
        } catch (error) {
            console.error('Error occurred while deleting user:', error);
            reply.code(500).send({ error: 'Internal Server Error' }); 
        }
    });
    
    };
