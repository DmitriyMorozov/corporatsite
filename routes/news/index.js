const fp = require('fastify-plugin');
const fs = require('fs');
const multipart = require('@fastify/multipart');
const NewsDAO = require('../../services/newsDAO');

module.exports = async function (fastify, opts) {
    fastify.register(multipart);
    fastify.register(NewsDAO);
    // fastify.addHook('preHandler', fastify.authenticate);  
    fastify.setErrorHandler(async (error, request, reply) => {
        console.error('Внутренняя ошибка сервера:', error);
        reply.code(500).send({ error: 'Внутренняя ошибка сервера', message: error.message, stack: error.stack });
    });

    fastify.post('/', { preValidation: [fastify.authenticate] }, async (req, reply) => {
        try {
            // Выводим токен, пришедший с запросом
            // console.log('Token:', req.headers.authorization);
           
            const userRoleId = req.user.role_id;
            
            // Проверяем, имеет ли пользователь роль администратора
            if (userRoleId !== 1) {
                reply.code(403).send({ error: 'Forbidden', message: 'Only admins can create news' });
                return;
            }
    
            const { title, content, created_at, author_id, image } = req.body;
    
            console.log('Received create request for news');
            console.log('News data:', { title, content, created_at, author_id, image });
    
            const createdNews = await fastify.newsDAO.createNews(title, content, created_at, author_id, image);
    
            console.log('News created successfully:', createdNews);
    
            // Возвращаем созданную новость
            return createdNews;
        } catch (error) {
            console.error('Error occurred while creating news:', error);
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });
    

    
    
    
    
    fastify.get('/', async (request, reply) => {
        try {
            const news = await fastify.newsDAO.getAllNews();
            return news;
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    fastify.get('/:news_id', async (request, reply) => {
        try {
            const { news_id } = request.params;
            const news = await fastify.newsDAO.getNewsById(news_id);
            if (!news) {
                reply.code(404).send({ error: 'News not found' });
            }
            return news;
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    fastify.put('/:news_id', async (request, reply) => {
        try {
            const { news_id } = request.params;
            const { title, content, created_at, author_id, image } = request.body; 
    
            console.log('Received update request for news with ID:', news_id);
            console.log('Update data:', { title, content, created_at, author_id, image });
    
            const updatedNews = await fastify.newsDAO.updateNews(news_id, title, content, created_at, author_id, image);
    
            console.log('News updated successfully:', updatedNews);
    
            return updatedNews;
        } catch (error) {
            console.error('Error occurred while updating news:', error);
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    
    fastify.delete('/:news_id', async (request, reply) => {
    try {
        const { news_id } = request.params;
        await fastify.newsDAO.deleteNews(news_id); 
        reply.status(204).send();
    } catch (error) {
        console.error('Error occurred while deleting news:', error);
        reply.code(500).send({ error: 'Internal Server Error' });
    }
});

};


const streamToString = async (stream) => {
    const chunks = [];
    for await (const chunk of stream) {
        chunks.push(chunk);
    }
    return Buffer.concat(chunks).toString('utf8');
};