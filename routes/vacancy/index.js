const fp = require('fastify-plugin');

module.exports = async function (fastify, opts) {
    // Регистрация вашего VacancyDAO
    fastify.register(require('../../services/vacancyDAO'));

    // Маршруты для управления вакансиями

    // Создание новой вакансии
    fastify.post('/', async (request, reply) => {
        try {
            const { title, description, date, hh_link } = request.body;
            const newVacancy = await fastify.vacancyDAO.createVacancy(title, description, date, hh_link);
            reply.code(201).send(newVacancy);
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    // Получение всех вакансий
    fastify.get('/', async (request, reply) => {
        try {
            const vacancies = await fastify.vacancyDAO.getAllVacancies();
            return vacancies;
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    // Получение вакансии по её ID
    fastify.get('/:vacancy_id', async (request, reply) => {
        try {
            const { vacancy_id } = request.params;
            const vacancy = await fastify.vacancyDAO.getVacancyById(vacancy_id);
            if (!vacancy) {
                reply.code(404).send({ error: 'Vacancy not found' });
            }
            return vacancy;
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    // Обновление существующей вакансии
    fastify.put('/:vacancy_id', async (request, reply) => {
        try {
            const { vacancy_id } = request.params;
            const { title, description, date, hh_link } = request.body;
            const updatedVacancy = await fastify.vacancyDAO.updateVacancy(vacancy_id, title, description, date, hh_link);
            return updatedVacancy;
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    // Удаление вакансии
    fastify.delete('/:vacancy_id', async (request, reply) => {
        try {
            const { vacancy_id } = request.params;
            await fastify.vacancyDAO.deleteVacancy(vacancy_id);
            reply.status(204).send(); 
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });
    
};
