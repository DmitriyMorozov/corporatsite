const fp = require('fastify-plugin');
const FurnitureTypeDAO = require('../../services/furnitureTypeDAO');

module.exports = async function (fastify, opts) {
    fastify.register(FurnitureTypeDAO);

    fastify.post('/', async (req, reply) => {
        try {
            const { name } = req.body;
            const createdType = await fastify.furnitureTypeDAO.createFurnitureType(name);
            return createdType;
        } catch (error) {
            console.error('Error occurred while creating furniture type:', error);
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    fastify.get('/', async (req, reply) => {
        try {
            const types = await fastify.furnitureTypeDAO.getAllFurnitureTypes();
            return types;
        } catch (error) {
            console.error('Error occurred while fetching furniture types:', error);
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    fastify.get('/:type_id', async (req, reply) => {
        try {
            const { type_id } = req.params;
            const type = await fastify.furnitureTypeDAO.getFurnitureTypeById(type_id);
            if (!type) {
                reply.code(404).send({ error: 'Furniture type not found' });
            }
            return type;
        } catch (error) {
            console.error('Error occurred while fetching furniture type by ID:', error);
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    fastify.put('/:type_id', async (req, reply) => {
        try {
            const { type_id } = req.params;
            const { name } = req.body;
            const updatedType = await fastify.furnitureTypeDAO.updateFurnitureType(type_id, name);
            return updatedType;
        } catch (error) {
            console.error('Произошла ошибка при обновлении типа мебели:', error);
            reply.code(500).send({ error: 'Внутренняя ошибка сервера' });
        }
    });

    fastify.delete('/:type_id', async (req, reply) => {
        try {
            const { type_id } = req.params;
            await fastify.furnitureTypeDAO.deleteFurnitureType(type_id);
            reply.status(204).send();
        } catch (error) {
            console.error('Error occurred while deleting furniture type:', error);
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });
};
