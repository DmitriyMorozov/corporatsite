const statusDAOPlugin = require('../../services/statusDAO');

module.exports = async function (fastify, opts) {

    fastify.register(statusDAOPlugin);

    // Routes for managing statuses
    fastify.get('/', async (request, reply) => {
        const statuses = await fastify.statusDAO.getAllStatuses();
        return statuses;
    });

    fastify.get('/:status_id', async (request, reply) => {
        const { status_id } = request.params;
        const status = await fastify.statusDAO.getStatusById(status_id);
        return status;
    });

    fastify.post('/', async (request, reply) => {
        const { name } = request.body;
        const newStatus = await fastify.statusDAO.createStatus(name);
        reply.code(201).send(newStatus);
    });

    fastify.put('/:status_id', async (request, reply) => {
        const { status_id } = request.params;
        const { name } = request.body;
        const updatedStatus = await fastify.statusDAO.updateStatus(status_id, name);
        return updatedStatus;
    });

    fastify.delete('/:status_id', async (request, reply) => {
        const { status_id } = request.params;
        await fastify.statusDAO.deleteStatus(status_id);
        reply.status(204).send(); // Send response with no content
    });
};
