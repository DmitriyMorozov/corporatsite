const fp = require('fastify-plugin');
const fs = require('fs');
const multipart = require('@fastify/multipart');
const CatalogDAO = require('../../services/catalogDAO');

module.exports = async function (fastify, opts) {
    fastify.register(multipart);
    fastify.register(CatalogDAO);
    // fastify.addHook('preHandler', fastify.authenticate);  
    fastify.setErrorHandler(async (error, request, reply) => {
        console.error('Внутренняя ошибка сервера:', error);
        reply.code(500).send({ error: 'Внутренняя ошибка сервера', message: error.message, stack: error.stack });
    });

    fastify.post('/', { preValidation: [fastify.authenticate] }, async (req, reply) => {
        try {
            // Выводим токен, пришедший с запросом
            console.log('Token:', req.headers.authorization);
            
            const userRoleId = req.user.role_id;
            
            // Проверяем, имеет ли пользователь роль администратора
            if (userRoleId !== 1) {
                reply.code(403).send({ error: 'Forbidden', message: 'Only admins can create products' });
                return;
            }
    
            const { name, description, type_id, image } = req.body;
    
            console.log('Received create request for product');
            console.log('Product data:', { name, description, type_id, image });
    
            const createdProduct = await fastify.catalogDAO.createProduct(name, description, type_id, image);
    
            console.log('Product created successfully:', createdProduct);
    
            // Возвращаем созданный продукт
            return createdProduct;
        } catch (error) {
            console.error('Error occurred while creating product:', error);
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });
    

    fastify.get('/type/:type_id', async (request, reply) => {
        try {
            const { type_id } = request.params;
            const products = await fastify.catalogDAO.getProductsByType(type_id);
            return products;
        } catch (error) {
            console.error('Error occurred while fetching products by type:', error);
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });
    
    
    
    
    fastify.get('/', async (request, reply) => {
        try {
            const products = await fastify.catalogDAO.getAllProducts();
            return products;
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    fastify.get('/:product_id', async (request, reply) => {
        try {
            const { product_id } = request.params;
            const product = await fastify.catalogDAO.getProductById(product_id);
            if (!product) {
                reply.code(404).send({ error: 'Product not found' });
            }
            return product;
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    fastify.put('/:product_id', async (request, reply) => {
        try {
            const { product_id } = request.params;
            const { name, description, type_id, image } = request.body; 
    
            console.log('Received update request for product with ID:', product_id);
            console.log('Update data:', { name, description, type_id, image });
    
            const updatedProduct = await fastify.catalogDAO.updateProduct(product_id, name, description, type_id, image);
    
            console.log('Product updated successfully:', updatedProduct);
    
            return updatedProduct;
        } catch (error) {
            console.error('Error occurred while updating product:', error);
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    fastify.delete('/:product_id', async (request, reply) => {
        try {
            const { product_id } = request.params;
            await fastify.catalogDAO.deleteProduct(product_id);
            reply.status(204).send();
        } catch (error) {
            console.error('Error occurred while deleting product:', error);
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });
    
};


const streamToString = async (stream) => {
    const chunks = [];
    for await (const chunk of stream) {
        chunks.push(chunk);
    }
    return Buffer.concat(chunks).toString('utf8');
};
