const tasksDAOPlugin = require('../../services/tasksDAO');

module.exports = async function (fastify, opts) {
    fastify.register(require('@fastify/multipart'));

    // Регистрируем плагин для работы с задачами
    fastify.register(tasksDAOPlugin);

    // Получение всех задач
    fastify.get('/', async (request, reply) => {
        try {
            const tasks = await fastify.taskDAO.getAllTasks();
            console.log('GET /tasks - Response:', tasks); // Логирование ответа
            return tasks;
        } catch (error) {
            console.error('GET /tasks - Error:', error); // Логирование ошибки
            // Обработка ошибки сервера
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    // Получение задачи по её ID
    fastify.get('/:task_id', async (request, reply) => {
        try {
            const { task_id } = request.params;
            const task = await fastify.taskDAO.getTaskById(task_id);
            console.log(`GET /tasks/${task_id} - Response:`, task); // Логирование ответа
            if (!task) {
                // Если задача не найдена, возвращаем код 404
                reply.code(404).send({ error: 'Task not found' });
            }
            return task;
        } catch (error) {
            console.error(`GET /tasks/:task_id - Error:`, error); // Логирование ошибки
            // Обработка ошибки сервера
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    // Создание новой задачи
    fastify.post('/', async (request, reply) => {
        try {
            const { title, description, deadline, created_by, start_date, status_id } = request.body;
            console.log('POST /tasks - Request Body:', request.body); // Логирование тела запроса
            console.log('POST /tasks - Data received:', { title, description, deadline, created_by, start_date, status_id }); // Логирование полученных данных
            const newTask = await fastify.taskDAO.createTask(title, description, deadline, created_by, start_date, status_id);
            console.log('POST /tasks - New Task:', newTask); // Логирование созданной задачи
            // Возвращаем созданную задачу и код 201
            reply.code(201).send(newTask);
        } catch (error) {
            console.error('POST /tasks - Error:', error); // Логирование ошибки
            // Обработка ошибки сервера
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    // Обновление существующей задачи
    fastify.put('/:task_id', async (request, reply) => {
        try {
            const { task_id } = request.params;
            const { title, description, deadline, created_by, start_date, status_id } = request.body;
            console.log(`PUT /tasks/${task_id} - Request Body:`, request.body); // Логирование тела запроса
            console.log(`PUT /tasks/${task_id} - Data received:`, { title, description, deadline, created_by, start_date, status_id }); // Логирование полученных данных
            const updatedTask = await fastify.taskDAO.updateTask(task_id, title, description, deadline, created_by, start_date, status_id);
            console.log(`PUT /tasks/${task_id} - Updated Task:`, updatedTask); // Логирование обновленной задачи
            return updatedTask;
        } catch (error) {
            console.error(`PUT /tasks/:task_id - Error:`, error); // Логирование ошибки
            // Обработка ошибки сервера
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    // Удаление задачи
    fastify.delete('/:task_id', async (request, reply) => {
        try {
            const { task_id } = request.params;
            console.log(`DELETE /tasks/${task_id} - Request Parameters:`, request.params); // Логирование параметров запроса
            await fastify.taskDAO.deleteTask(task_id);
            console.log(`DELETE /tasks/${task_id} - Deleted Task ID:`, task_id); // Логирование удаленной задачи
            // Отправляем код 204, так как контент удален
            reply.status(204).send();
        } catch (error) {
            console.error(`DELETE /tasks/:task_id - Error:`, error); // Логирование ошибки
            // Обработка ошибки сервера
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });
    
    // Получение задач пользователя по его ID
    fastify.get('/user/:created_by', async (request, reply) => {
        try {
            const { created_by } = request.params;
            const tasks = await fastify.taskDAO.getTasksByUserId(created_by);
            console.log(`GET /tasks/user/${created_by} - Response:`, tasks); // Логирование ответа
            return tasks;
        } catch (error) {
            console.error(`GET /tasks/user/:user_id - Error:`, error); // Логирование ошибки
            // Обработка ошибки сервера
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

};
