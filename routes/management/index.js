const managementDAOPlugin = require('../../services/managementDAO');

module.exports = async function (fastify, opts) {

    fastify.register(managementDAOPlugin);

    // Routes for managing management entries
    fastify.get('/', async (request, reply) => {
        const entries = await fastify.managementDAO.getAllManagementEntries();
        return entries;
    });

    fastify.get('/:management_id', async (request, reply) => {
        const { management_id } = request.params;
        const entry = await fastify.managementDAO.getManagementEntryById(management_id);
        return entry;
    });

    fastify.post('/', async (request, reply) => {
        const { description, user_id } = request.body;
        const newEntry = await fastify.managementDAO.createManagement(description, user_id);
        reply.code(201).send(newEntry);
    });

    fastify.put('/:management_id', async (request, reply) => {
        const { management_id } = request.params;
        const { description, user_id } = request.body;
        const updatedEntry = await fastify.managementDAO.updateManagementEntry(management_id, description, user_id);
        return updatedEntry;
    });

    fastify.delete('/:management_id', async (request, reply) => {
        const { management_id } = request.params;
        await fastify.managementDAO.deleteManagementEntry(management_id);
        reply.status(204).send(); // Send response with no content
    });
};
