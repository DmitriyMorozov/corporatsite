const fp = require('fastify-plugin');
const fs = require('fs');
const multipart = require('@fastify/multipart');
const BlogDAO = require('../../services/blogDAO');

module.exports = async function (fastify, opts) {
    fastify.register(multipart, { attachFieldsToBody: true });  

    fastify.post('/', async (request, reply) => {
        try {
            const { title, content, author_id, image, created_at } = request.body;
    
            console.log('Received create request for post');
            console.log('Post data:', { title, content, author_id, image, created_at });
    
            const createdPost = await fastify.blogDAO.createPost(title, content, author_id, image, created_at);
    
            console.log('Post created successfully:', createdPost);
    
            return createdPost;
        } catch (error) {
            console.error('Error occurred while creating post:', error);
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });
    
    
    fastify.get('/', async (request, reply) => {
        try {
            const posts = await fastify.blogDAO.getAllPosts();
            return posts;
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    fastify.get('/:post_id', async (request, reply) => {
        try {
            const { post_id } = request.params;
            const post = await fastify.blogDAO.getPostById(post_id);
            if (!post) {
                reply.code(404).send({ error: 'Post not found' });
            }
            return post;
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    fastify.put('/:post_id', async (request, reply) => {
        try {
            const { post_id } = request.params;
            const { title, content, author_id, image, created_at } = request.body; 
    
            console.log('Received update request for post with ID:', post_id);
            console.log('Update data:', { title, content, author_id, image, created_at });
    
            const updatedPost = await fastify.blogDAO.updatePost(post_id, title, content, author_id, image, created_at);
    
            console.log('Post updated successfully:', updatedPost);
    
            return updatedPost;
        } catch (error) {
            console.error('Error occurred while updating post:', error);
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    fastify.delete('/:post_id', async (request, reply) => {
        try {
            const { post_id } = request.params;
            await fastify.blogDAO.deletePost(post_id);
            reply.code(204).send(); 
        } catch (error) {
            console.error('Error occurred while deleting user:', error);
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });
    
};
