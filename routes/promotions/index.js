const promotionsDAOPlugin = require('../../services/promotionsDAO');
const multipart = require('@fastify/multipart');
const fp = require('fastify-plugin');
const fs = require('fs');

module.exports = async function (fastify, opts) {
    // Регистрируем плагин для работы с акциями
    fastify.register(promotionsDAOPlugin);

    // Получение всех акций
    fastify.get('/', async (request, reply) => {
        try {
            const promotions = await fastify.promotionsDAO.getAllPromotions();
            console.log('GET /promotions - Response:', promotions); // Логирование ответа
            return promotions;
        } catch (error) {
            console.error('GET /promotions - Error:', error); // Логирование ошибки
            // Обработка ошибки сервера
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    // Получение акции по её ID
    fastify.get('/:promotion_id', async (request, reply) => {
        try {
            const { promotion_id } = request.params;
            const promotion = await fastify.promotionsDAO.getPromotionById(promotion_id);
            console.log(`GET /promotions/${promotion_id} - Response:`, promotion); // Логирование ответа
            if (!promotion) {
                // Если акция не найдена, возвращаем код 404
                reply.code(404).send({ error: 'Promotion not found' });
            } else {
                return promotion;
            }
        } catch (error) {
            console.error(`GET /promotions/:promotion_id - Error:`, error); // Логирование ошибки
            // Обработка ошибки сервера
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    // Создание новой акции
    fastify.post('/', async (request, reply) => {
        try {
            const { name, description, start_date, end_date, image } = request.body;
            console.log('POST /promotions - Request Body:', request.body); // Логирование тела запроса
            console.log('POST /promotions - Data received:', { name, description, start_date, end_date, image }); // Логирование полученных данных
            const newPromotion = await fastify.promotionsDAO.createPromotion(name, description, start_date, end_date, image);
            console.log('POST /promotions - New Promotion:', newPromotion); // Логирование созданной акции
            // Возвращаем созданную акцию и код 201
            reply.code(201).send(newPromotion);
        } catch (error) {
            console.error('POST /promotions - Error:', error); // Логирование ошибки
            // Обработка ошибки сервера
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    // Обновление существующей акции
    fastify.put('/:promotion_id', async (request, reply) => {
        try {
            const { promotion_id } = request.params;
            const { name, description, start_date, end_date, image } = request.body; 
    
            console.log('Received update request for promotion with ID:', promotion_id);
            console.log('Update data:', { name, description, start_date, end_date, image });
    
            const updatedPromotion = await fastify.promotionsDAO.updatePromotion(promotion_id, name, description, start_date, end_date, image);
    
            console.log('Promotion updated successfully:', updatedPromotion);
    
            return updatedPromotion;
        } catch (error) {
            console.error('Error occurred while updating promotion:', error);
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    // Удаление акции по её ID
    fastify.delete('/:promotion_id', async (request, reply) => {
        try {
            const { promotion_id } = request.params;
            console.log(`DELETE /promotions/${promotion_id} - Request Parameters:`, request.params); // Логирование параметров запроса
            await fastify.promotionsDAO.deletePromotion(promotion_id);
            console.log(`DELETE /promotions/${promotion_id} - Promotion deleted successfully`); // Логирование успешного удаления акции
            // Возвращаем код 204 (No Content) - акция успешно удалена
            reply.code(204).send();
        } catch (error) {
            console.error(`DELETE /promotions/:promotion_id - Error:`, error); // Логирование ошибки
            // Обработка ошибки сервера
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });
};
