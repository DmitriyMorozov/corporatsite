const rolesDAOPlugin = require('../../services/rolesDAO');

module.exports = async function (fastify, opts) {

    fastify.register(rolesDAOPlugin);

    // Маршруты для управления ролями
    fastify.get('/', async (request, reply) => {
        const roles = await fastify.roleDAO.getAllRoles();
        return roles;
    });

    fastify.get('/:role_id', async (request, reply) => {
        const { role_id } = request.params;
        const role = await fastify.roleDAO.getRoleById(role_id);
        return role;
    });

    fastify.post('/', async (request, reply) => {
        const { role_name } = request.body;
        const newRole = await fastify.roleDAO.createRole(role_name);
        reply.code(201).send(newRole);
    });

    fastify.put('/:role_id', async (request, reply) => {
        const { role_id } = request.params;
        const { role_name } = request.body;
        const updatedRole = await fastify.roleDAO.updateRole(role_id, role_name);
        return updatedRole;
    });

    fastify.delete('/:role_id', async (request, reply) => {
        const { role_id } = request.params;
        await fastify.roleDAO.deleteRole(role_id);
        reply.status(204).send(); // Отправляем ответ без содержимого
    });
};
