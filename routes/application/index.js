const applicationDAOPlugin = require('../../services/applicationsDAO');

module.exports = async function (fastify, opts) {
    fastify.register(require('@fastify/multipart'));

    // Register the plugin for working with applications
    fastify.register(applicationDAOPlugin);

    // Routes for managing applications
    fastify.get('/', async (request, reply) => {
        try {
            const applications = await fastify.applicationDAO.getAllApplications();
            return applications;
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    fastify.get('/:application_id', async (request, reply) => {
        try {
            const { application_id } = request.params;
            const application = await fastify.applicationDAO.getApplicationById(application_id);
            if (!application) {
                reply.code(404).send({ error: 'Application not found' });
            }
            return application;
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    

    fastify.post('/', async (request, reply) => {
        try {
            const { username, phone, email, comment } = request.body;
            const newApplication = await fastify.applicationDAO.createApplication(username, phone, email, comment);
            reply.code(201).send(newApplication);
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    fastify.put('/:application_id', async (request, reply) => {
        try {
            const { application_id } = request.params;
            const { username, phone, email, comment } = request.body;
            const updatedApplication = await fastify.applicationDAO.updateApplication(application_id, username, phone, email, comment);
            return updatedApplication;
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    fastify.delete('/:application_id', async (request, reply) => {
        try {
            const { application_id } = request.params;
            await fastify.applicationDAO.deleteApplication(application_id);
            reply.status(204).send(); // Send response with no content
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });
};
