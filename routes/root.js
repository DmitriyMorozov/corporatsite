'use strict';

const jwt = require('@fastify/jwt');
const configuration = require('../config/configuration');
const roles = require('./roles');

module.exports = async function (fastify, opts) {
    const bcrypt = require('bcrypt');
    const usersDAO = fastify.userDAO; // Получаем экземпляр объекта userDAO из Fastify
    const tokensDAO = fastify.tokensDAO; // Получаем экземпляр объекта tokensDAO из Fastify
    
    fastify.post('/login', async (req, reply) => {
        try {
            const { email, password } = req.body;
                
            // Проверяем, существует ли пользователь с указанным email в базе данных
            const user = await usersDAO.getUserByEmail(email);
            if (!user) {
                throw new Error('Invalid credentials');
            }
    
            // Сравниваем введенный пароль с хэшированным паролем пользователя из базы данных
            const isPasswordValid = await bcrypt.compare(password, user.password);
            if (!isPasswordValid) {
                throw new Error('Invalid credentials');
            }
    
            // Генерируем уникальный токен, включая информацию о ролях пользователя
            let token = fastify.jwt.sign({
                email,
                user_id: user.user_id, // Включаем user_id в токен
                role_id: user.role_id, // Включаем role_id в токен
                username: user.username,
                exp: Math.floor(Date.now() / 1000) + (60 * 60 * 15) // Устанавливаем срок действия токена
            });
    
            // Если учетные данные верны, создаем и отправляем JWT токен клиенту
            fastify.log.info(`Успешная аутентификация для пользователя с email: ${email} и role_id: ${user.role_id}`);
            reply.send({ token, user_id: user.user_id, role_id: user.role_id}); // Передаем user_id в ответе
        } catch (error) {
            fastify.log.error(`Ошибка аутентификации: ${error.message}`);
            reply.code(401).send({ error: 'Authentication failed', error_detailed: error.message });
        }
    });
    
    
    
    fastify.get(
        '/',
        {
            preValidation: [fastify.authenticate]
        },
        async function (request, reply) {
            const userRoles = request.user.roles; // Получаем роли аутентифицированного пользователя
            // В зависимости от ролей пользователя предоставляем доступ к данным
            if (userRoles.includes('admin')) {
               
            } else {
                
            }
            const userEmail = request.user.email; // Получаем email аутентифицированного пользователя
            fastify.log.info(`Успешный доступ к защищенному маршруту для пользователя с email: ${userEmail}`);
            return request.user;
        }
    );
    

    fastify.options('/login', async (req, reply) => {
        reply.code(200).send();
    });

    // Вспомогательная функция для генерации токена
    function generateToken() {
        return Math.random().toString(36).substr(2); 
    }
};
