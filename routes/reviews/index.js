const reviewsDAOPlugin = require('../../services/reviewsDAO');

module.exports = async function (fastify, opts) {
    fastify.register(require('@fastify/multipart'));

    // Register the plugin for working with reviews
    fastify.register(reviewsDAOPlugin);

    // Routes for managing reviews
    fastify.get('/', async (request, reply) => {
        try {
            const reviews = await fastify.reviewsDAO.getAllReviews();
            return reviews;
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    fastify.get('/:review_id', async (request, reply) => {
        try {
            const { review_id } = request.params;
            const review = await fastify.reviewsDAO.getReviewById(review_id);
            if (!review) {
                reply.code(404).send({ error: 'Review not found' });
            }
            return review;
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    

    fastify.post('/', async (request, reply) => {
        try {
            const { user_id, review_text, review_date, position_name, company_name, rating } = request.body;
            const newReview = await fastify.reviewsDAO.createReview(user_id, review_text, review_date, position_name, company_name, rating);
            reply.code(201).send(newReview);
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });
    

    fastify.put('/:review_id', async (request, reply) => {
        try {
            const { review_id } = request.params;
            const { user_id, review_text, review_date, position_name, company_name, rating } = request.body;
            const updatedReview = await fastify.reviewsDAO.updateReview(review_id, user_id, review_text, review_date, position_name, company_name, rating);
            return updatedReview;
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    fastify.delete('/:review_id', async (request, reply) => {
        try {
            const { review_id } = request.params;
            await fastify.reviewsDAO.deleteReview(review_id);
            reply.status(204).send(); // Send response with no content
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });
};
