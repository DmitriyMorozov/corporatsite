const fp = require('fastify-plugin');
const fs = require('fs');
const multipart = require('@fastify/multipart');
const ServicesDAO = require('../../services/servicesDAO');

module.exports = async function (fastify, opts) {
    fastify.register(multipart);
    fastify.register(ServicesDAO);
    // fastify.addHook('preHandler', fastify.authenticate);  
    fastify.setErrorHandler(async (error, request, reply) => {
        console.error('Внутренняя ошибка сервера:', error);
        reply.code(500).send({ error: 'Внутренняя ошибка сервера', message: error.message, stack: error.stack });
    });

    fastify.post('/', { preValidation: [fastify.authenticate] }, async (req, reply) => {
        try {
            // Выводим токен, пришедший с запросом
            // console.log('Token:', req.headers.authorization);
           
            const userRoleId = req.user.role_id;
            
            // Проверяем, имеет ли пользователь роль администратора
            if (userRoleId !== 1) {
                reply.code(403).send({ error: 'Forbidden', message: 'Only admins can create services' });
                return;
            }
    
            const { name, description, image } = req.body;
    
            console.log('Received create request for service');
            console.log('Service data:', { name, description, image });
    
            const createdService = await fastify.servicesDAO.createService(name, description, image);
    
            console.log('Service created successfully:', createdService);
    
            // Возвращаем созданную услугу
            return createdService;
        } catch (error) {
            console.error('Error occurred while creating service:', error);
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });
    

    
    
    
    
    fastify.get('/', async (request, reply) => {
        try {
            const services = await fastify.servicesDAO.getAllServices();
            return services;
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    fastify.get('/:services_id', async (request, reply) => {
        try {
            const { services_id } = request.params;
            const service = await fastify.servicesDAO.getServiceById(services_id);
            if (!service) {
                reply.code(404).send({ error: 'Service not found' });
            }
            return service;
        } catch (error) {
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    fastify.put('/:services_id', async (request, reply) => {
        try {
            const { services_id } = request.params;
            const { name, description, image } = request.body; 
    
            console.log('Received update request for service with ID:', services_id);
            console.log('Update data:', { name, description, image });
    
            const updatedService = await fastify.servicesDAO.updateService(services_id, name, description, image);
    
            console.log('Service updated successfully:', updatedService);
    
            return updatedService;
        } catch (error) {
            console.error('Error occurred while updating service:', error);
            reply.code(500).send({ error: 'Internal Server Error' });
        }
    });

    
    fastify.delete('/:services_id', async (request, reply) => {
    try {
        const { services_id } = request.params;
        await fastify.servicesDAO.deleteService(services_id); 
        reply.status(204).send();
    } catch (error) {
        console.error('Error occurred while deleting service:', error);
        reply.code(500).send({ error: 'Internal Server Error' });
    }
});

};


const streamToString = async (stream) => {
    const chunks = [];
    for await (const chunk of stream) {
        chunks.push(chunk);
    }
    return Buffer.concat(chunks).toString('utf8');
};
