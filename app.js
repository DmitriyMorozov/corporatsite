'use strict';

const path = require('node:path');
const AutoLoad = require('@fastify/autoload');
const fastify = require('fastify')();
const fastifyWebSocket = require('@fastify/websocket');


const options = {}
/**
 * 
 * @param {import('fastify').FastifyInstance} fastify 
 * @param {{}} opts
 */
module.exports = async function (fastify, opts) {
  
  await fastify.register(require('@fastify/cors'), {
    origin: true  
  });

  fastify.addHook('preHandler', (req, reply, done) => {
    
    const corsOptions = {
      origin: true  
    };

    if (/^localhost$/m.test(req.headers.origin)) {
      corsOptions.origin = false;
    }

    done();
  });

  
  await fastify.register(AutoLoad, {
    dir: path.join(__dirname, 'plugins'),
    options: Object.assign({}, opts)
  })

  await fastify.register(AutoLoad, {
    dir: path.join(__dirname, 'services'),
    options: Object.assign({}, opts)
  })

 
  await fastify.register(AutoLoad, {
    dir: path.join(__dirname, 'routes'),
    options: Object.assign({}, opts)
  })
}

module.exports.options = options
