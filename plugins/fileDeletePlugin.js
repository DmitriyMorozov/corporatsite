const fs = require('fs');

/**
 * @param {import('fastify').FastifyInstance} fastify 
 * @param {*} options 
 */
function deleteFile(fastify, options, done) {
    /**
     * @param {import('fastify').FastifyRequest} request 
     * @param {import('fastify').FastifyReply} reply 
     */
    fastify.delete('/delete-image/:filename', (request, reply) => {
        const filename = request.params.filename;
        const filePath = `./static/${filename}`;

        // Проверяем, существует ли файл
        fs.access(filePath, fs.constants.F_OK, (err) => {
            if (err) {
                reply.code(404).send({ error: 'File not found' });
                console.error(`File '${filename}' not found.`);
            } else {
                // Удаляем файл
                fs.unlink(filePath, (err) => {
                    if (err) {
                        reply.code(500).send({ error: 'Failed to delete file' });
                        console.error(`Failed to delete file '${filename}':`, err);
                    } else {
                        reply.send({ message: 'File deleted successfully' });
                        console.log(`File '${filename}' deleted successfully.`);
                    }
                });
            }
        });
    });

    done();
}

module.exports = deleteFile;
