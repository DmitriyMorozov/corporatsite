// // // fileUploadPlugin.js

// const fs = require('fs');
// const path = require('path');

// async function fileUploadPlugin(fastify, options) {
//     fastify.register(require('@fastify/multipart'));

//     fastify.post('/upload', async (request, reply) => {
//         const data = await request.file();
//         const filePath = path.join(__dirname, 'static', data.filename);

//         const writeStream = fs.createWriteStream(filePath);
//         data.file.pipe(writeStream);

//         return 'File uploaded successfully!';
//     });
// }

// module.exports = fileUploadPlugin;
