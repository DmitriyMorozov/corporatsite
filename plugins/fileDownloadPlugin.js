const fs = require('fs');

/**
 * 
 * @param {import('fastify').FastifyInstance} fastify 
 * @param {*} options 
 */
function getFile(fastify, options, done) {
    fastify.get('/static/*', (request, reply) => {
        const dir = fs.readdirSync('./static');
        if (dir.includes(request.url.split('/')[2])) {
            const file = fs.readFileSync('.' + request.url);
            reply.send(file);
        } else {
            reply.code(404).send();
            console.log(`Файл '${'.' + request.url}' не существует или нет доступа к нему.`);
        }
    })

    done();
}

module.exports = getFile;