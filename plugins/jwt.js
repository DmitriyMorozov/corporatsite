const fp = require('fastify-plugin');
const configuration = require('../config/configuration');

module.exports = fp(async function (fastify, opts, done) {

    // Регистрируем и настраиваем fastify-jwt для аутентификации
    fastify.register(require('@fastify/jwt'), {
        secret: configuration.secretKey
    });

    // Добавляем декоратор authenticate для предварительной валидации запросов
    fastify.decorate("authenticate", async function (request, reply) {
        try {
            // Проверяем JWT токен
            const dataFromToken = await request.jwtVerify();

            //Проверяем время жизни токена
            if (+(dataFromToken.l + dataFromToken.iat + '000') < Date.now()) throw new Error(`Token has been dead`);

        } catch (err) {
            // В случае ошибки аутентификации отправляем ответ с кодом 401 и сообщением об ошибке
            reply.code(401).send({ error: 'Authentication failed', message: err.message });
            return;
        }
    });

    done(); // Вызываем done() для завершения регистрации плагина
});
