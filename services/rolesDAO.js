const fp = require('fastify-plugin');

const RolesDAO = (db) => {
    const createRole = async (newRoleName) => {
        const { role_id, role_name } = await db.one('INSERT INTO roles (role_name) VALUES ($1) RETURNING role_id, role_name',
            [newRoleName]
        );
    
        return { role_id, role_name };
    };
    

    const getAllRoles = () => {
        const roles = db.any('SELECT role_id, role_name FROM roles');

        return roles;
    };

    const getRoleById = async (role_id) => {
        try {
            const role = await db.one('SELECT role_id, role_name FROM roles WHERE role_id = $1', [role_id]);
            return role;
        } catch (error) {
            throw new Error('Role not found');
        }
    };

    const updateRole = async (role_id, role_name) => {
        try {
            await db.one(
                'UPDATE roles SET role_name = $1 WHERE role_id = $2 RETURNING role_id',
                [role_name, role_id]
            );
            return { role_id, role_name };
        } catch (error) {
            throw new Error('Update failed');
        }
    };

    const deleteRole = async (role_id) => {
        await db.query('DELETE FROM roles WHERE role_id = $1', [role_id]);
    };

    return { createRole, getAllRoles, getRoleById, updateRole, deleteRole };
};

module.exports = fp((fastify, options, next) => {
    //@ts-ignore
    fastify.decorate('roleDAO', RolesDAO(fastify.db));
    next();
});
