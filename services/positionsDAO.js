const fp = require('fastify-plugin');

const PositionsDAO = (db) => {
    const createPosition = async (position_name) => {
        try {
            const { id } = await db.one(
                'INSERT INTO positions (position_name) VALUES ($1) RETURNING id',
                [position_name]
            );
            return { id, position_name };
        } catch (error) {
            throw new Error('Failed to create position');
        }
    };

    const getAllPositions = async () => {
        try {
            const positions = await db.any('SELECT id, position_name FROM positions');
            return positions;
        } catch (error) {
            throw new Error('Failed to fetch positions');
        }
    };

    const getPositionById = async (position_id) => {
        try {
            const position = await db.oneOrNone('SELECT id, position_name FROM positions WHERE id = $1', [position_id]);
            if (!position) {
                throw new Error('Position not found');
            }
            return position;
        } catch (error) {
            throw new Error('Failed to fetch position');
        }
    };

    const updatePosition = async (position_id, position_name) => {
        try {
            await db.none(
                'UPDATE positions SET position_name = $1 WHERE id = $2',
                [position_name, position_id]
            );
            return { id: position_id, position_name };
        } catch (error) {
            throw new Error('Failed to update position');
        }
    };
    

    const deletePosition = async (position_id) => {
        try {
            await db.none('DELETE FROM positions WHERE id = $1', [position_id]);
        } catch (error) {
            throw new Error('Failed to delete position');
        }
    };

    return { createPosition, getAllPositions, getPositionById, updatePosition, deletePosition };
};

module.exports = fp((fastify, options, next) => {
    fastify.decorate('positionsDAO', PositionsDAO(fastify.db));
    next();
});
