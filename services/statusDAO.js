const fp = require('fastify-plugin');

const StatusDAO = (db) => {
    const createStatus = async (newStatusName) => {
        const { status_id, name } = await db.one('INSERT INTO status (name) VALUES ($1) RETURNING status_id, name',
            [newStatusName]
        );

        return { status_id, name };
    };


    const getAllStatuses = async () => {
        const statuses = await db.any('SELECT status_id, name FROM status');
        return statuses;
    };

    const getStatusById = async (status_id) => {
        try {
            const status = await db.one('SELECT status_id, name FROM status WHERE status_id = $1', [status_id]);
            return status;
        } catch (error) {
            throw new Error('Status not found');
        }
    };

    const updateStatus = async (status_id, name) => {
        try {
            await db.one(
                'UPDATE status SET name = $1 WHERE status_id = $2 RETURNING status_id',
                [name, status_id]
            );
            return { status_id, name };
        } catch (error) {
            throw new Error('Update failed');
        }
    };

    const deleteStatus = async (status_id) => {
        await db.query('DELETE FROM status WHERE status_id = $1', [status_id]);
    };

    return { createStatus, getAllStatuses, getStatusById, updateStatus, deleteStatus };
};

module.exports = fp((fastify, options, next) => {
    //@ts-ignore
    fastify.decorate('statusDAO', StatusDAO(fastify.db));
    next();
});
