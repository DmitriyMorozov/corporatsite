const fp = require('fastify-plugin');
const fs = require('fs');
const { v4: uuidv4 } = require('uuid');

/**
 * @param {import('pg-promise').IDatabase<{}, import('pg').IClient>} db
 */
const ServicesDAO = (db) => {
    /**
     * @param {String} fileType
     */
    const generateUniqueFileName = (fileType) => {
        const timestamp = Date.now();
        const randomString = Math.random().toString(36).substring(2, 8);
        return `${timestamp}-${randomString}.${fileType}`;
    };

    /**
     * @param {String} image
     */
    const createService = async (name, description, image) => {
        try {
            console.log('Creating service...');
            console.log('Service data:', { name, description, image });

            let image_url = null;
            let uniqueFileName = null; // Объявляем переменную здесь

            if (image) {
                console.log('Processing image:', image);

                // Логика сохранения изображения
                const dirPath = './static';
                if (!fs.existsSync(dirPath)) {
                    fs.mkdirSync(dirPath);
                }

                const match = image.match(/\/([a-zA-Z]*)\;/);
                const fileType = match ? match[1] : null;

                uniqueFileName = generateUniqueFileName(fileType); // Присваиваем значение внутри условия

                const base64Data = image.replace(/^data:image\/\w+;base64,/, '');
                const buffer = Buffer.from(base64Data, 'base64');

                fs.writeFile(`${dirPath}/${uniqueFileName}`, buffer, (err) => {
                    if (err) {
                        console.error('Error while saving image:', err);
                        throw new Error('Error while saving image.');
                    } else {
                        console.log('Image saved successfully!');
                    }
                });
            }

            console.log('Inserting service into the database...');
            await db.none(
                'INSERT INTO services (name, description, image_url) VALUES ($1, $2, $3)',
                [name, description, uniqueFileName] // Передаем значение uniqueFileName
            );

            console.log('Service created successfully');

            return { name, description, image_url: uniqueFileName }; // Используем uniqueFileName здесь
        } catch (error) {
            console.error('Error occurred while creating service:', error);
            throw new Error('Failed to create service');
        }
    };

    const getAllServices = async () => {
        try {
            const services = await db.any('SELECT services_id, name, description, image_url FROM services');
            return services;
        } catch (error) {
            throw new Error('Failed to fetch services');
        }
    };

    const getServiceById = async (services_id) => {
        try {
            const service = await db.oneOrNone('SELECT services_id, name, description, image_url FROM services WHERE services_id = $1', [services_id]);
            if (!service) {
                throw new Error('Service not found');
            }
            return service;
        } catch (error) {
            throw new Error('Failed to fetch service');
        }
    };

    const deleteService = async (services_id) => {
        try {
            console.log('Deleting service with ID:', services_id);
            const service = await db.one('SELECT * FROM services WHERE services_id = $1', [services_id]);
            if (!service) {
                throw new Error('Service not found');
            }

            if (service.image_url) {
                const filePath = `./static/${service.image_url}`;
                // Удаляем файл изображения, если он существует
                fs.access(filePath, fs.constants.F_OK, (err) => {
                    if (!err) {
                        fs.unlink(filePath, (err) => {
                            if (err) {
                                console.error('Error while deleting file:', err);
                            } else {
                                console.log('File deleted successfully:', filePath);
                            }
                        });
                    }
                });
            }

            await db.none('DELETE FROM services WHERE services_id = $1', [services_id]);
            console.log('Service deleted successfully');
        } catch (error) {
            console.error('Error occurred while deleting service:', error);
            throw new Error('Failed to delete service');
        }
    };

    const updateService = async (services_id, name, description, image) => {
        try {
            console.log('Updating service with ID:', services_id);
            console.log('Update data:', { name, description, image });

            let image_url = null;

            if (image) {
                console.log('Processing image:', image);

                // Логика сохранения изображения
                const dirPath = './static';
                if (!fs.existsSync(dirPath)) {
                    fs.mkdirSync(dirPath);
                }

                const match = image.match(/\/([a-zA-Z]*)\;/);
                const fileType = match ? match[1] : null;

                image_url = generateUniqueFileName(fileType);

                const base64Data = image.replace(/^data:image\/\w+;base64,/, '');
                const buffer = Buffer.from(base64Data, 'base64');

                fs.writeFile(`${dirPath}/${image_url}`, buffer, (err) => {
                    if (err) {
                        console.error('Error while saving image:', err);
                        throw new Error('Error while saving image.');
                    } else {
                        console.log('Image saved successfully!');
                    }
                });
            }

            console.log('Updating service in the database...');
            await db.none(
                'UPDATE services SET name = $1, description = $2, image_url = $3 WHERE services_id = $4',
                [name, description, image_url, services_id]
            );

            console.log('Service updated successfully');

            return { services_id, name, description, image_url };
        } catch (error) {
            console.error('Error occurred while updating service:', error);
            throw new Error('Failed to update service');
        }
    };

    return { createService, getAllServices, getServiceById, updateService, deleteService };
};

module.exports = fp((fastify, options, next) => {
    //@ts-ignore
    fastify.decorate('servicesDAO', ServicesDAO(fastify.db));
    next();
});
