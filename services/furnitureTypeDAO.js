const fp = require('fastify-plugin');

const FurnitureTypeDAO = (db) => {
    const createFurnitureType = async (name) => {
        try {
            console.log('Creating furniture type...');
            console.log('Furniture type data:', { name });
    
            const result = await db.one(
                'INSERT INTO furniture_type (name) VALUES ($1) RETURNING *',
                [name]
            );
    
            console.log('Furniture type created successfully:', result);
    
            return result;
        } catch (error) {
            console.error('Error occurred while creating furniture type:', error);
            throw new Error('Failed to create furniture type');
        }
    };
    

    const getAllFurnitureTypes = async () => {
        try {
            const types = await db.any('SELECT * FROM furniture_type');
            return types;
        } catch (error) {
            console.error('Error occurred while fetching furniture types:', error);
            throw new Error('Failed to fetch furniture types');
        }
    };

    const getFurnitureTypeById = async (type_id) => {
        try {
            const type = await db.oneOrNone('SELECT * FROM furniture_type WHERE type_id = $1', [type_id]);
            if (!type) {
                throw new Error('Furniture type not found');
            }
            return type;
        } catch (error) {
            console.error('Error occurred while fetching furniture type by ID:', error);
            throw new Error('Failed to fetch furniture type');
        }
    };

    const updateFurnitureType = async (type_id, name) => {
        try {
            console.log('Обновление типа мебели с ID:', type_id);
            console.log('Данные для обновления:', { name });
    
            const result = await db.one(
                'UPDATE furniture_type SET name = $1 WHERE type_id = $2 RETURNING *',
                [name, type_id]
            );
    
            console.log('Тип мебели успешно обновлен:', result);
    
            return result;
        } catch (error) {
            console.error('Произошла ошибка при обновлении типа мебели:', error);
            throw new Error('Не удалось обновить тип мебели');
        }
    };

    const deleteFurnitureType = async (type_id) => {
        try {
            console.log('Deleting furniture type with ID:', type_id);
            await db.none('DELETE FROM furniture_type WHERE type_id = $1', [type_id]);
            console.log('Furniture type deleted successfully');
        } catch (error) {
            console.error('Error occurred while deleting furniture type:', error);
            throw new Error('Failed to delete furniture type');
        }
    };

    return { createFurnitureType, getAllFurnitureTypes, getFurnitureTypeById, updateFurnitureType, deleteFurnitureType };
};

module.exports = fp((fastify, options, next) => {
    fastify.decorate('furnitureTypeDAO', FurnitureTypeDAO(fastify.db));
    next();
});
