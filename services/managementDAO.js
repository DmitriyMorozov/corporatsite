const fp = require('fastify-plugin');

const ManagementDAO = (db) => {
    const createManagement = async (description, user_id) => {
        try {
            const { management_id } = await db.one(
                'INSERT INTO management (description, user_id) VALUES ($1, $2) RETURNING management_id',
                [description, user_id]
            );
            return { management_id, description, user_id };
        } catch (error) {
            throw new Error('Failed to create management entry');
        }
    };

    const getAllManagementEntries = async () => {
        try {
            const entries = await db.any('SELECT management_id, description, user_id FROM management');
            return entries;
        } catch (error) {
            throw new Error('Failed to fetch management entries');
        }
    };

    const getManagementEntryById = async (management_id) => {
        try {
            const entry = await db.oneOrNone('SELECT management_id, description, user_id FROM management WHERE management_id = $1', [management_id]);
            if (!entry) {
                throw new Error('Management entry not found');
            }
            return entry;
        } catch (error) {
            throw new Error('Failed to fetch management entry');
        }
    };

    const updateManagementEntry = async (management_id, description, user_id) => {
        try {
            await db.none(
                'UPDATE management SET description = $1, user_id = $2 WHERE management_id = $3',
                [description, user_id, management_id]
            );
            return { management_id, description, user_id };
        } catch (error) {
            throw new Error('Failed to update management entry');
        }
    };

    const deleteManagementEntry = async (management_id) => {
        try {
            await db.none('DELETE FROM management WHERE management_id = $1', [management_id]);
        } catch (error) {
            throw new Error('Failed to delete management entry');
        }
    };

    return { createManagement, getAllManagementEntries, getManagementEntryById, updateManagementEntry, deleteManagementEntry };
};

module.exports = fp((fastify, options, next) => {
    fastify.decorate('managementDAO', ManagementDAO(fastify.db));
    next();
});
