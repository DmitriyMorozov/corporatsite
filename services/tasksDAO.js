const fp = require('fastify-plugin');
const fs = require('fs');
const { v4: uuidv4 } = require('uuid');

/**
 * @param {import('pg-promise').IDatabase<{}, import('pg').IClient>} db
 */
const TaskDAO = (db) => {
    /**
     * Генерирует уникальное имя файла на основе типа файла.
     * @param {String} fileType Тип файла.
     * @returns {String} Уникальное имя файла.
     */
    const generateUniqueFileName = (fileType) => {
        const timestamp = Date.now();
        const randomString = Math.random().toString(36).substring(2, 8);
        return `${timestamp}-${randomString}${fileType}`;
    };

    /**
     * Создает задачу.
     * @param {String} title Заголовок задачи.
     * @param {String} description Описание задачи.
     * @param {Date} deadline Дата и время завершения задачи.
     * @param {Number} created_by Идентификатор пользователя, создавшего задачу.
     * @param {Date} start_date Дата начала выполнения задачи.
     * @param {Number} status_id Идентификатор статуса задачи.
     * @returns {Promise<any>} Созданная задача.
     */
    const createTask = async (title, description, deadline, created_by, start_date, status_id) => {
        try {
            console.log('Creating task...');
            console.log('Task data:', { title, description, deadline, created_by, start_date, status_id });
    
            console.log('Inserting task into the database...');
            await db.none(
                'INSERT INTO tasks (title, description, deadline, created_by, start_date, status_id) VALUES ($1, $2, $3, $4, $5, $6)',
                [title, description, deadline, created_by, start_date, status_id]
            );
    
            console.log('Task created successfully');
    
            return { title, description, deadline, created_by, start_date, status_id };
        } catch (error) {
            console.error('Error occurred while creating task:', error);
            throw new Error('Failed to create task');
        }
    };

    /**
     * Получает все задачи.
     * @returns {Promise<any>} Массив задач.
     */
    const getAllTasks = async () => {
        try {
            const tasks = await db.any('SELECT task_id, title, description, deadline, created_by, start_date, status_id FROM tasks');
            return tasks;
        } catch (error) {
            throw new Error('Failed to fetch tasks');
        }
    };

    /**
     * Получает задачу по идентификатору.
     * @param {Number} task_id Идентификатор задачи.
     * @returns {Promise<any>} Найденная задача или null, если задача не найдена.
     */
    const getTaskById = async (task_id) => {
        try {
            const task = await db.oneOrNone('SELECT task_id, title, description, deadline, created_by, start_date, status_id FROM tasks WHERE task_id = $1', [task_id]);
            if (!task) {
                throw new Error('Task not found');
            }
            return task;
        } catch (error) {
            throw new Error('Failed to fetch task');
        }
    };

    /**
     * Обновляет задачу.
     * @param {Number} task_id Идентификатор задачи.
     * @param {String} title Заголовок задачи.
     * @param {String} description Описание задачи.
     * @param {Date} deadline Дата и время завершения задачи.
     * @param {Number} created_by Идентификатор пользователя, создавшего задачу.
     * @param {Date} start_date Дата начала выполнения задачи.
     * @param {Number} status_id Идентификатор статуса задачи.
     * @returns {Promise<any>} Обновленная задача.
     */
    const updateTask = async (task_id, title, description, deadline, created_by, start_date, status_id) => {
        try {
            console.log('Updating task with ID:', task_id);
            console.log('Update data:', { title, description, deadline, created_by, start_date, status_id });
    
            console.log('Updating task in the database...');
            await db.none(
                'UPDATE tasks SET title = $1, description = $2, deadline = $3, created_by = $4, start_date = $5, status_id = $6 WHERE task_id = $7',
                [title, description, deadline, created_by, start_date, status_id, task_id]
            );
    
            console.log('Task updated successfully');
    
            return { task_id, title, description, deadline, created_by, start_date, status_id };
        } catch (error) {
            console.error('Error occurred while updating task:', error);
            throw new Error('Failed to update task');
        }
    };

    /**
     * Удаляет задачу.
     * @param {Number} task_id Идентификатор задачи.
     * @returns {Promise<void>} Результат удаления.
     */
    const deleteTask = async (task_id) => {
        try {
            console.log('Deleting task with ID:', task_id);
            await db.none('DELETE FROM tasks WHERE task_id = $1', [task_id]);
            console.log('Task deleted successfully');
        } catch (error) {
            console.error('Error occurred while deleting task:', error);
            throw new Error('Failed to delete task');
        }
    };

    /**
     * Получает все задачи для определенного пользователя.
     * @param {Number} created_by Идентификатор пользователя.
     * @returns {Promise<any>} Массив задач для пользователя.
     */
    const getTasksByUserId = async (created_by) => {
        try {
            const tasks = await db.any('SELECT task_id, title, description, deadline, created_by, start_date, status_id FROM tasks WHERE created_by = $1', [created_by]);
            return tasks;
        } catch (error) {
            throw new Error('Failed to fetch tasks for user');
        }
    };

    return { createTask, getAllTasks, getTaskById, updateTask, deleteTask, getTasksByUserId };

};

module.exports = fp((fastify, options, next) => {
    //@ts-ignore
    fastify.decorate('taskDAO', TaskDAO(fastify.db));
    next();
});
