const fp = require('fastify-plugin');

const VacancyDAO = (db) => {
    const createVacancy = async (title, description, date, hh_link) => {
        try {
            const { vacancy_id } = await db.one(
                'INSERT INTO vacancy (title, description, date, hh_link) VALUES ($1, $2, $3, $4) RETURNING vacancy_id',
                [title, description, date, hh_link]
            );
            return { vacancy_id, title, description, date, hh_link };
        } catch (error) {
            throw new Error('Failed to create vacancy');
        }
    };

    const getAllVacancies = async () => {
        try {
            const vacancies = await db.any('SELECT vacancy_id, title, description, date, hh_link FROM vacancy');
            return vacancies;
        } catch (error) {
            throw new Error('Failed to fetch vacancies');
        }
    };

    const getVacancyById = async (vacancy_id) => {
        try {
            const vacancy = await db.oneOrNone('SELECT vacancy_id, title, description, date, hh_link FROM vacancy WHERE vacancy_id = $1', [vacancy_id]);
            if (!vacancy) {
                throw new Error('Vacancy not found');
            }
            return vacancy;
        } catch (error) {
            throw new Error('Failed to fetch vacancy');
        }
    };

    const updateVacancy = async (vacancy_id, title, description, date, hh_link) => {
        try {
            await db.none(
                'UPDATE vacancy SET title = $1, description = $2, date = $3, hh_link = $4 WHERE vacancy_id = $5',
                [title, description, date, hh_link, vacancy_id]
            );
            return { vacancy_id, title, description, date, hh_link };
        } catch (error) {
            throw new Error('Failed to update vacancy');
        }
    };

    const deleteVacancy = async (vacancy_id) => {
        try {
            await db.none('DELETE FROM vacancy WHERE vacancy_id = $1', [vacancy_id]);
        } catch (error) {
            throw new Error('Failed to delete vacancy');
        }
    };
    
    return { createVacancy, getAllVacancies, getVacancyById, updateVacancy, deleteVacancy };
};

module.exports = fp((fastify, options, next) => {
    fastify.decorate('vacancyDAO', VacancyDAO(fastify.db));
    next();
});
