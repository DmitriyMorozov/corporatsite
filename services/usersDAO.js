const bcrypt = require('bcrypt');
const { Console } = require('console');
const fs = require('fs');
const fp = require('fastify-plugin');
const { v4: uuidv4 } = require('uuid');

/**
 * @typedef {{
 *      username: string
 *      email: string
 *      password: string
 *      role_id: number
 *      birth_date: Date
 *      avatar: string
 *      position_id: number
 * }} User
 */

/**
 * @param {import('pg-promise').IDatabase<{}, import('pg').IClient>} db
 */
const UsersDAO = (db) => {
    const createUser = async (username, email, password, role_id, birth_date, avatar, position_id) => {
        try {
            let avatar_url = null;
        
            if (avatar) {
                // Если передано новое изображение, обрабатываем его
                const dir = fs.readdirSync('./static');

                const fileType = (avatar.match(/\/([a-zA-Z]*)\;/))[1];

                avatar_url = uniqueFileName() + '.' + fileType;

                while (dir.includes(avatar_url)) avatar_url = uniqueFileName() + '.' + fileType;

                const base64Data = avatar.replace(/^data:image\/\w+;base64,/, '');
                const buffer = Buffer.from(base64Data, 'base64');

                fs.writeFile('./static/' + avatar_url, buffer, (err) => {
                    if (err) {
                        console.error('Ошибка при сохранении изображения:', err);
                    } else {
                        console.log('Изображение сохранено успешно!');
                    }
                });
            }

            console.log('User paassDAO:', password);
            const hashedPassword = await bcrypt.hash(password, 10);
            const { user_id } = await db.one(
                'INSERT INTO users (username, email, password, role_id, birth_date, avatar_url, position_id) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING user_id',
                [username, email, hashedPassword, role_id, birth_date, avatar_url, position_id]
            );
            return { user_id, username, email, role_id, birth_date, avatar: avatar_url, position_id };
        } catch (error) {
            throw new Error('Failed to create user');
        }
    };

    const getAllUsers = async () => {
        try {
            const users = await db.any('SELECT user_id, username, email, role_id, birth_date, avatar_url, position_id FROM users');
            return users;
        } catch (error) {
            throw new Error('Failed to fetch users');
        }
    };

    const getUserById = async (user_id) => {
        try {
            const user = await db.oneOrNone('SELECT user_id, username, email, role_id, password, birth_date, avatar_url, position_id FROM users WHERE user_id = $1', [user_id]);
            if (!user) {
                throw new Error('User not found');
            }
            return user;
        } catch (error) {
            throw new Error('Failed to fetch user');
        }
    };

    const deleteUser = async (user_id) => {
        try {
            console.log('Deleting user with ID:', user_id);
            // Delete user from database
            await db.none('DELETE FROM users WHERE user_id = $1', [user_id]);
            console.log('User deleted successfully');
    
            // Delete avatar if exists
            const user = await db.oneOrNone('SELECT * FROM users WHERE user_id = $1', [user_id]);
            if (user && user.avatar_url) {
                const imagePath = `./static/${user.avatar_url}`;
                fs.unlink(imagePath, (err) => {
                    if (err) {
                        console.error('Error while deleting avatar:', err);
                    } else {
                        console.log('Avatar deleted successfully:', imagePath);
                    }
                });
            }
        } catch (error) {
            console.error('Error occurred while deleting user:', error);
            throw new Error('Failed to delete user');
        }
    };
    
    
    const updateUser = async (user_id, username, email, password, role_id, birth_date, avatar, position_id) => {
        try {
            let avatar_url = null;
    
            if (avatar) {
                // Если передано новое изображение, обрабатываем его
                const dir = fs.readdirSync('./static');
    
                const fileType = (avatar.match(/\/([a-zA-Z]*)\;/))[1];
    
                avatar_url = uniqueFileName() + '.' + fileType;
    
                while (dir.includes(avatar_url)) avatar_url = uniqueFileName() + '.' + fileType;
    
                const base64Data = avatar.replace(/^data:image\/\w+;base64,/, '');
                const buffer = Buffer.from(base64Data, 'base64');
    
                fs.writeFile('./static/' + avatar_url, buffer, (err) => {
                    if (err) {
                        console.error('Ошибка при сохранении изображения:', err);
                    } else {
                        console.log('Изображение сохранено успешно!');
                    }
                });
    
                // Удаляем старую аватарку, если она существует
                const user = await db.oneOrNone('SELECT * FROM users WHERE user_id = $1', [user_id]);
                if (user && user.avatar_url) {
                    const oldImagePath = `./static/${user.avatar_url}`;
                    fs.unlink(oldImagePath, (err) => {
                        if (err) {
                            console.error('Error while deleting old avatar:', err);
                        } else {
                            console.log('Old avatar deleted successfully:', oldImagePath);
                        }
                    });
                }
            }
    
            const hashedPassword = await bcrypt.hash(password, 10);
            await db.none(
                'UPDATE users SET username = $1, email = $2, password = $3, role_id = $4, birth_date = $5, avatar_url = $6, position_id = $7 WHERE user_id = $8',
                [username, email, hashedPassword, role_id, birth_date, avatar_url, position_id, user_id]
            );
            return { user_id, username, email, role_id, birth_date, avatar: avatar_url, position_id };
        } catch (error) {
            throw new Error('Failed to update user');
        }
    };

    /**
     * @param {String} user_email
     * @returns {Promise<User>}
     */
    const getUserByEmail = async (user_email) => {
        try {
            return await db.one('SELECT * FROM Users WHERE email = $1', [user_email]);
        } catch (error) {
            throw new Error('Failed to find user by email: ' + error);
        }
    };


    return { createUser, getAllUsers, getUserById, updateUser, deleteUser, getUserByEmail };
};

module.exports = fp((fastify, options, next) => {
    //@ts-ignore
    fastify.decorate('userDAO', UsersDAO(fastify.db));
    next();
});

const uniqueFileName = () => {
    const timestamp = Date.now();
    const randomString = Math.random().toString(36).substring(2, 8); // Генерация случайной строки
    return `${timestamp}-${randomString}`;
}
