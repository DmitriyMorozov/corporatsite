const fp = require('fastify-plugin');
const fs = require('fs');
const { v4: uuidv4 } = require('uuid');

/**
 * @param {import('pg-promise').IDatabase<{}, import('pg').IClient>} db
 */
const PromotionsDAO = (db) => {
    /**
     * Генерирует уникальное имя файла на основе типа файла.
     * @param {String} fileType Тип файла.
     * @returns {String} Уникальное имя файла.
     */
    const generateUniqueFileName = (fileType) => {
        const timestamp = Date.now();
        const randomString = Math.random().toString(36).substring(2, 8);
        return `${timestamp}-${randomString}.${fileType}`;
    };

    /**
     * Создает акцию.
     * @param {String} name Название акции.
     * @param {String} description Описание акции.
     * @param {Date} start_date Дата начала акции.
     * @param {Date} end_date Дата окончания акции.
     * @param {String} image Изображение акции в формате base64.
     * @returns {Promise<any>} Созданная акция.
     */
    const createPromotion = async (name, description, start_date, end_date, image) => {
        try {
            console.log('Creating promotion...');
            console.log('Promotion data:', { name, description, start_date, end_date });

            let uniqueFileName = null;

            if (image) {
                console.log('Processing image:', image);

                // Логика сохранения изображения
                const dirPath = './static';
                if (!fs.existsSync(dirPath)) {
                    fs.mkdirSync(dirPath);
                }

                const match = image.match(/\/([a-zA-Z]*)\;/);
                const fileType = match ? match[1] : null;

                uniqueFileName = generateUniqueFileName(fileType);

                const base64Data = image.replace(/^data:image\/\w+;base64,/, '');
                const buffer = Buffer.from(base64Data, 'base64');

                fs.writeFile(`${dirPath}/${uniqueFileName}`, buffer, (err) => {
                    if (err) {
                        console.error('Error while saving image:', err);
                        throw new Error('Error while saving image.');
                    } else {
                        console.log('Image saved successfully!');
                    }
                });
            }

            console.log('Inserting promotion into the database...');
            await db.none(
                'INSERT INTO promotions (name, description, start_date, end_date, image_url) VALUES ($1, $2, $3, $4, $5)',
                [name, description, start_date, end_date, uniqueFileName]
            );

            console.log('Promotion created successfully');

            return { name, description, start_date, end_date, image_url: uniqueFileName };
        } catch (error) {
            console.error('Error occurred while creating promotion:', error);
            throw new Error('Failed to create promotion');
        }
    };

    /**
     * Получает все акции.
     * @returns {Promise<any>} Массив акций.
     */
    const getAllPromotions = async () => {
        try {
            const promotions = await db.any('SELECT promotion_id, name, description, start_date, end_date, image_url FROM promotions');
            return promotions;
        } catch (error) {
            throw new Error('Failed to fetch promotions');
        }
    };

    /**
     * Получает акцию по идентификатору.
     * @param {Number} promotion_id Идентификатор акции.
     * @returns {Promise<any>} Найденная акция или null, если акция не найдена.
     */
    const getPromotionById = async (promotion_id) => {
        try {
            const promotion = await db.oneOrNone('SELECT promotion_id, name, description, start_date, end_date, image_url FROM promotions WHERE promotion_id = $1', [promotion_id]);
            if (!promotion) {
                throw new Error('Promotion not found');
            }
            return promotion;
        } catch (error) {
            throw new Error('Failed to fetch promotion');
        }
    };

   /**
 * Обновляет акцию.
 * @param {Number} promotion_id Идентификатор акции.
 * @param {String} name Название акции.
 * @param {String} description Описание акции.
 * @param {Date} start_date Дата начала акции.
 * @param {Date} end_date Дата окончания акции.
 * @param {String} image Изображение акции в формате base64.
 * @returns {Promise<any>} Обновленная акция.
 */
const updatePromotion = async (promotion_id, name, description, start_date, end_date, image) => {
    try {
        console.log('Creating promotion...');
        console.log('Promotion data:', { name, description, start_date, end_date });

        let uniqueFileName = null;

        if (image) {
            console.log('Processing image:', image);

            // Логика сохранения изображения
            const dirPath = './static';
            if (!fs.existsSync(dirPath)) {
                fs.mkdirSync(dirPath);
            }

            const match = image.match(/\/([a-zA-Z]*)\;/);
            const fileType = match ? match[1] : null;

            uniqueFileName = generateUniqueFileName(fileType);

            const base64Data = image.replace(/^data:image\/\w+;base64,/, '');
            const buffer = Buffer.from(base64Data, 'base64');

            fs.writeFile(`${dirPath}/${uniqueFileName}`, buffer, (err) => {
                if (err) {
                    console.error('Error while saving image:', err);
                    throw new Error('Error while saving image.');
                } else {
                    console.log('Image saved successfully!');
                }
            });
        }

        console.log('Inserting promotion into the database...');
        await db.none(
            'UPDATE promotions SET name = $1, description = $2, start_date = $3, end_date = $4, image_url = $5 WHERE promotion_id = $6',
            [name, description, start_date, end_date, uniqueFileName, promotion_id]
        );

        console.log('Promotion created successfully');

        return { name, description, start_date, end_date, image_url: uniqueFileName };
    } catch (error) {
        console.error('Error occurred while creating promotion:', error);
        throw new Error('Failed to create promotion');
    }
};

    /**
     * Удаляет акцию.
     * @param {Number} promotion_id Идентификатор акции.
     * @returns {Promise<void>} Промис без возвращаемого значения.
     */
    const deletePromotion = async (promotion_id) => {
        try {
            console.log('Deleting promotion with ID:', promotion_id);
            const promotion = await db.one('SELECT * FROM promotions WHERE promotion_id = $1', [promotion_id]);
            if (!promotion) {
                throw new Error('Promotion not found');
            }

            if (promotion.image_url) {
                const filePath = `./static/${promotion.image_url}`;
                // Удаляем файл изображения, если он существует
                fs.access(filePath, fs.constants.F_OK, (err) => {
                    if (!err) {
                        fs.unlink(filePath, (err) => {
                            if (err) {
                                console.error('Error while deleting file:', err);
                            } else {
                                console.log('File deleted successfully:', filePath);
                            }
                        });
                    }
                });
            }

            await db.none('DELETE FROM promotions WHERE promotion_id = $1', [promotion_id]);
            console.log('Promotion deleted successfully');
        } catch (error) {
            console.error('Error occurred while deleting promotion:', error);
            throw new Error('Failed to delete promotion');
        }
    };

    // Добавляем методы к объекту PromotionsDAO
    return { createPromotion, getAllPromotions, getPromotionById, updatePromotion, deletePromotion };
};

module.exports = fp((fastify, options, next) => {
    //@ts-expect-error
    fastify.decorate('promotionsDAO', PromotionsDAO(fastify.db));
    next();
});
