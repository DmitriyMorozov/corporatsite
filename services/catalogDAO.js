const fp = require('fastify-plugin');
const fs = require('fs');

/**
 * @param {import('pg-promise').IDatabase<{}, import('pg').IClient>} db
 */
const CatalogDAO = (db) => {
    /**
     * @param {String} fileType
     */
    const generateUniqueFileName = (fileType) => {
        const timestamp = Date.now();
        const randomString = Math.random().toString(36).substring(2, 8);
        return `${timestamp}-${randomString}.${fileType}`;
    };

    /**
     * @param {String} image
     */
    const createProduct = async (name, description, type_id, image) => {
        try {
            console.log('Creating product...');
            console.log('Product data:', { name, description, type_id, image });
    
            let uniqueFileName = null;
    
            if (image) {
                console.log('Processing image:', image);
    
                // Логика сохранения изображения
                const dirPath = './static';
                if (!fs.existsSync(dirPath)) {
                    fs.mkdirSync(dirPath);
                }
    
                const match = image.match(/\/([a-zA-Z]*)\;/);
                const fileType = match ? match[1] : null;
    
                uniqueFileName = generateUniqueFileName(fileType);
    
                const base64Data = image.replace(/^data:image\/\w+;base64,/, '');
                const buffer = Buffer.from(base64Data, 'base64');
    
                fs.writeFile(`${dirPath}/${uniqueFileName}`, buffer, (err) => {
                    if (err) {
                        console.error('Error while saving image:', err);
                        throw new Error('Error while saving image.');
                    } else {
                        console.log('Image saved successfully!');
                    }
                });
            }
    
            console.log('Inserting product into the database...');
            await db.none(
                'INSERT INTO catalog (name, description, type_id, image_url) VALUES ($1, $2, $3, $4)',
                [name, description, type_id, uniqueFileName]
            );
    
            console.log('Product created successfully');
    
            return { name, description, type_id, image_url: uniqueFileName };
        } catch (error) {
            console.error('Error occurred while creating product:', error);
            throw new Error('Failed to create product');
        }
    };

    const getAllProducts = async () => {
        try {
            const products = await db.any('SELECT product_id, name, description, type_id, image_url FROM catalog');
            return products;
        } catch (error) {
            throw new Error('Failed to fetch products');
        }
    };

    const getProductById = async (product_id) => {
        try {
            const product = await db.oneOrNone('SELECT product_id, name, description, type_id, image_url FROM catalog WHERE product_id = $1', [product_id]);
            if (!product) {
                throw new Error('Product not found');
            }
            return product;
        } catch (error) {
            throw new Error('Failed to fetch product');
        }
    };
    
    /**
     * @param {String} type_id
     */
    const getProductsByType = async (type_id) => {
        try {
            const products = await db.any('SELECT product_id, name, description, type_id, image_url FROM catalog WHERE type_id = $1', [type_id]);
            return products;
        } catch (error) {
            throw new Error('Failed to fetch products by type');
        }
    };

    /**
     * @param {String} product_id
     */
    const deleteProduct = async (product_id) => {
        try {
            console.log('Deleting product with ID:', product_id);
            const product = await db.one('SELECT * FROM catalog WHERE product_id = $1', [product_id]);
            if (!product) {
                throw new Error('Product not found');
            }

            if (product.image_url) {
                const filePath = `./static/${product.image_url}`;
                // Удаляем файл изображения, если он существует
                fs.access(filePath, fs.constants.F_OK, (err) => {
                    if (!err) {
                        fs.unlink(filePath, (err) => {
                            if (err) {
                                console.error('Error while deleting file:', err);
                            } else {
                                console.log('File deleted successfully:', filePath);
                            }
                        });
                    }
                });
            }

            await db.none('DELETE FROM catalog WHERE product_id = $1', [product_id]);
            console.log('Product deleted successfully');
        } catch (error) {
            console.error('Error occurred while deleting product:', error);
            throw new Error('Failed to delete product');
        }
    };

    const updateProduct = async (product_id, name, description, type_id, image) => {
        try {
            console.log('Updating product with ID:', product_id);
            console.log('Update data:', { name, description, type_id, image });
    
            let uniqueFileName = null;
    
            if (image) {
                console.log('Processing image:', image);
    
                // Логика сохранения изображения
                const dirPath = './static';
                if (!fs.existsSync(dirPath)) {
                    fs.mkdirSync(dirPath);
                }
    
                const match = image.match(/\/([a-zA-Z]*)\;/);
                const fileType = match ? match[1] : null;
    
                uniqueFileName = generateUniqueFileName(fileType);
    
                const base64Data = image.replace(/^data:image\/\w+;base64,/, '');
                const buffer = Buffer.from(base64Data, 'base64');
    
                fs.writeFile(`${dirPath}/${uniqueFileName}`, buffer, (err) => {
                    if (err) {
                        console.error('Error while saving image:', err);
                        throw new Error('Error while saving image.');
                    } else {
                        console.log('Image saved successfully!');
                    }
                });
            }
    
            console.log('Updating product in the database...');
            await db.none(
                'UPDATE catalog SET name = $1, description = $2, type_id = $3, image_url = $4 WHERE product_id = $5',
                [name, description, type_id, uniqueFileName, product_id]
            );
    
            console.log('Product updated successfully');
    
            return { product_id, name, description, type_id, image_url: uniqueFileName };
        } catch (error) {
            console.error('Error occurred while updating product:', error);
            throw new Error('Failed to update product');
        }
    };

    // Добавляем метод getProductsByType к объекту catalogDAO
    return { createProduct, getAllProducts, getProductById, deleteProduct, updateProduct, getProductsByType };
};

module.exports = fp((fastify, options, next) => {
    fastify.decorate('catalogDAO', CatalogDAO(fastify.db));
    next();
});
