const fp = require('fastify-plugin');
const fs = require('fs');
const { v4: uuidv4 } = require('uuid');

/**
 * @param {import('pg-promise').IDatabase<{}, import('pg').IClient>} db
 */
const NewsDAO = (db) => {
    /**
     * @param {String} fileType
     */
    const generateUniqueFileName = (fileType) => {
        const timestamp = Date.now();
        const randomString = Math.random().toString(36).substring(2, 8);
        return `${timestamp}-${randomString}.${fileType}`;
    };

    /**
     * @param {String} image
     */
    const createNews = async (title, content, created_at, author_id, image) => {
        try {
            console.log('Creating news...');
            console.log('News data:', { title, content, created_at, author_id, image });
    
            let image_url = null;
            let uniqueFileName = null; // Объявляем переменную здесь
    
            if (image) {
                console.log('Processing image:', image);
    
                // Логика сохранения изображения
                const dirPath = './static';
                if (!fs.existsSync(dirPath)) {
                    fs.mkdirSync(dirPath);
                }
    
                const match = image.match(/\/([a-zA-Z]*)\;/);
                const fileType = match ? match[1] : null;
    
                uniqueFileName = generateUniqueFileName(fileType); // Присваиваем значение внутри условия
    
                const base64Data = image.replace(/^data:image\/\w+;base64,/, '');
                const buffer = Buffer.from(base64Data, 'base64');
    
                fs.writeFile(`${dirPath}/${uniqueFileName}`, buffer, (err) => {
                    if (err) {
                        console.error('Error while saving image:', err);
                        throw new Error('Error while saving image.');
                    } else {
                        console.log('Image saved successfully!');
                    }
                });
            }
    
            console.log('Inserting news into the database...');
            await db.none(
                'INSERT INTO news (title, content, created_at, author_id, image_url ) VALUES ($1, $2, $3, $4, $5)',
                [title, content, created_at, author_id, uniqueFileName] // Передаем значение uniqueFileName
            );
    
            console.log('News created successfully');
    
            return { title, content, created_at, author_id, image: uniqueFileName }; // Используем uniqueFileName здесь
        } catch (error) {
            console.error('Error occurred while creating news:', error);
            throw new Error('Failed to create news');
        }
    };
    
    
    
    
    
    const getAllNews = async () => {
        try {
            const news = await db.any('SELECT news_id, title, content, created_at, author_id, image_url FROM news');
            return news;
        } catch (error) {
            throw new Error('Failed to fetch news');
        }
    };

    const getNewsById = async (news_id) => {
        try {
            const news = await db.oneOrNone('SELECT news_id, title, content, created_at, author_id, image_url FROM news WHERE news_id = $1', [news_id]);
            if (!news) {
                throw new Error('News not found');
            }
            return news;
        } catch (error) {
            throw new Error('Failed to fetch news');
        }
    };

    const deleteNews = async (news_id) => {
        try {
            console.log('Deleting news with ID:', news_id);
            const news = await db.one('SELECT * FROM news WHERE news_id = $1', [news_id]);
            if (!news) {
                throw new Error('News not found');
            }
    
            if (news.image_url) {
                const filePath = `./static/${news.image_url}`;
                // Удаляем файл изображения, если он существует
                fs.access(filePath, fs.constants.F_OK, (err) => {
                    if (!err) {
                        fs.unlink(filePath, (err) => {
                            if (err) {
                                console.error('Error while deleting file:', err);
                            } else {
                                console.log('File deleted successfully:', filePath);
                            }
                        });
                    }
                });
            }
    
            await db.none('DELETE FROM news WHERE news_id = $1', [news_id]);
            console.log('News deleted successfully');
        } catch (error) {
            console.error('Error occurred while deleting news:', error);
            throw new Error('Failed to delete news');
        }
    };
    
    const updateNews = async (news_id, title, content, created_at, author_id, image ) => {
        try {
            console.log('Updating news with ID:', news_id);
            console.log('Update data:', { title, content, created_at, author_id, image  });
    
            let image_url = null;
    
            if (image) {
                console.log('Processing image:', image);
    
                // Логика сохранения изображения
                const dirPath = './static';
                if (!fs.existsSync(dirPath)) {
                    fs.mkdirSync(dirPath);
                }
    
                const match = image.match(/\/([a-zA-Z]*)\;/);
                const fileType = match ? match[1] : null;
    
                image_url = generateUniqueFileName(fileType);
    
                const base64Data = image.replace(/^data:image\/\w+;base64,/, '');
                const buffer = Buffer.from(base64Data, 'base64');
    
                fs.writeFile(`${dirPath}/${image_url}`, buffer, (err) => {
                    if (err) {
                        console.error('Error while saving image:', err);
                        throw new Error('Error while saving image.');
                    } else {
                        console.log('Image saved successfully!');
                    }
                });
            }
    
            console.log('Updating news in the database...');
            await db.none(
                'UPDATE news SET title = $1, content = $2, created_at = $3, author_id = $4, image_url = $5 WHERE news_id = $6',
                [title, content, created_at, author_id, image_url, news_id]
            );
    
            console.log('News updated successfully');
    
            return { news_id, title, content, created_at, author_id, image: image_url };
        } catch (error) {
            console.error('Error occurred while updating news:', error);
            throw new Error('Failed to update news');
        }
    };
    
    

    return { createNews, getAllNews, getNewsById, updateNews, deleteNews };
};

module.exports = fp((fastify, options, next) => {
    //@ts-ignore
    fastify.decorate('newsDAO', NewsDAO(fastify.db));
    next();
});
