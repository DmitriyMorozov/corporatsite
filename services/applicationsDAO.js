const fp = require('fastify-plugin');

const ApplicationDAO = (db) => {
    const createApplication = async (username, phone, email, comment) => {
        try {
            const { application_id } = await db.one(
                'INSERT INTO application (username, phone, email, comment) VALUES ($1, $2, $3, $4) RETURNING application_id',
                [username, phone, email, comment]
            );
            return { application_id, username, phone, email, comment };
        } catch (error) {
            throw new Error('Failed to create application');
        }
    };

    const getAllApplications = async () => {
        try {
            const applications = await db.any('SELECT application_id, username, phone, email, comment FROM application');
            return applications;
        } catch (error) {
            throw new Error('Failed to fetch applications');
        }
    };

    const getApplicationById = async (application_id) => {
        try {
            const application = await db.oneOrNone('SELECT application_id, username, phone, email, comment FROM application WHERE application_id = $1', [application_id]);
            if (!application) {
                throw new Error('Application not found');
            }
            return application;
        } catch (error) {
            throw new Error('Failed to fetch application');
        }
    };

    const updateApplication = async (application_id, username, phone, email, comment) => {
        try {
            await db.none(
                'UPDATE application SET username = $1, phone = $2, email = $3, comment = $4 WHERE application_id = $5',
                [username, phone, email, comment, application_id]
            );
            return { application_id, username, phone, email, comment };
        } catch (error) {
            throw new Error('Failed to update application');
        }
    };

    const deleteApplication = async (application_id) => {
        try {
            await db.none('DELETE FROM application WHERE application_id = $1', [application_id]);
        } catch (error) {
            throw new Error('Failed to delete application');
        }
    };

    return { createApplication, getAllApplications, getApplicationById, updateApplication, deleteApplication };
};

module.exports = fp((fastify, options, next) => {
    fastify.decorate('applicationDAO', ApplicationDAO(fastify.db));
    next();
});
