const fp = require('fastify-plugin');
const fs = require('fs');
const { v4: uuidv4 } = require('uuid');

/**
 * @param {import('pg-promise').IDatabase<{}, import('pg').IClient>} db
 */
const BlogDAO = (db) => {
    /**
     * @param {String} fileType
     */
    const generateUniqueFileName = (fileType) => {
        const timestamp = Date.now();
        const randomString = Math.random().toString(36).substring(2, 8);
        return `${timestamp}-${randomString}.${fileType}`;
    };

    /**
     * @param {String} image
     */
    const createPost = async (title, content, author_id, image, created_at) => {
        try {
            console.log('Creating post...');
            console.log('Post data:', { title, content, author_id, image, created_at });
    
            let image_url = null;
            let uniqueFileName = null; // Объявляем переменную здесь
    
            if (image) {
                console.log('Processing image:', image);
    
                // Логика сохранения изображения
                const dirPath = './static';
                if (!fs.existsSync(dirPath)) {
                    fs.mkdirSync(dirPath);
                }
    
                const match = image.match(/\/([a-zA-Z]*)\;/);
                const fileType = match ? match[1] : null;
    
                uniqueFileName = generateUniqueFileName(fileType); // Присваиваем значение внутри условия
    
                const base64Data = image.replace(/^data:image\/\w+;base64,/, '');
                const buffer = Buffer.from(base64Data, 'base64');
    
                fs.writeFile(`${dirPath}/${uniqueFileName}`, buffer, (err) => {
                    if (err) {
                        console.error('Error while saving image:', err);
                        throw new Error('Error while saving image.');
                    } else {
                        console.log('Image saved successfully!');
                    }
                });
            }
    
            console.log('Inserting post into the database...');
            await db.none(
                'INSERT INTO blog (title, content, author_id, image_url, created_at) VALUES ($1, $2, $3, $4, $5)',
                [title, content, author_id, uniqueFileName, created_at] // Передаем значение uniqueFileName
            );
    
            console.log('Post created successfully');
    
            return { title, content, author_id, image: uniqueFileName, created_at }; // Используем uniqueFileName здесь
        } catch (error) {
            console.error('Error occurred while creating post:', error);
            throw new Error('Failed to create post');
        }
    };
    
    const getAllPosts = async () => {
        try {
            const posts = await db.any('SELECT * FROM blog');
            return posts;
        } catch (error) {
            throw new Error('Failed to fetch posts');
        }
    };

    const getPostById = async (post_id) => {
        try {
            const post = await db.oneOrNone('SELECT * FROM blog WHERE post_id = $1', [post_id]);
            if (!post) {
                throw new Error('Post not found');
            }
            return post;
        } catch (error) {
            throw new Error('Failed to fetch post');
        }
    };

    const updatePost = async (post_id, title, content, author_id, image, created_at) => {
        try {
            console.log('Updating post with ID:', post_id);
            console.log('Update data:', { title, content, author_id, image, created_at });
    
            let image_url = null;
    
            if (image) {
                console.log('Processing image:', image);
    
                // Логика сохранения изображения
                const dirPath = './static';
                if (!fs.existsSync(dirPath)) {
                    fs.mkdirSync(dirPath);
                }
    
                const match = image.match(/\/([a-zA-Z]*)\;/);
                const fileType = match ? match[1] : null;
    
                image_url = generateUniqueFileName(fileType);
    
                const base64Data = image.replace(/^data:image\/\w+;base64,/, '');
                const buffer = Buffer.from(base64Data, 'base64');
    
                fs.writeFile(`${dirPath}/${image_url}`, buffer, (err) => {
                    if (err) {
                        console.error('Error while saving image:', err);
                        throw new Error('Error while saving image.');
                    } else {
                        console.log('Image saved successfully!');
                    }
                });
            }
    
            console.log('Updating post in the database...');
            await db.none(
                'UPDATE blog SET title = $1, content = $2, author_id = $3, image_url = $4, created_at = $5 WHERE post_id = $6',
                [title, content, author_id, image_url, created_at, post_id]
            );
    
            console.log('Post updated successfully');
    
            return { post_id, title, content, author_id, image: image_url, created_at };
        } catch (error) {
            console.error('Error occurred while updating post:', error);
            throw new Error('Failed to update post');
        }
    };

    const deletePost = async (post_id) => {
        try {
            console.log('Deleting post with ID:', post_id);
            // Получаем информацию о посте
            const post = await db.oneOrNone('SELECT * FROM blog WHERE post_id = $1', [post_id]);
            if (!post) {
                throw new Error('Post not found');
            }
    
            // Удаляем изображение, если оно существует
            if (post.image_url) {
                const imagePath = `./static/${post.image_url}`;
                fs.unlink(imagePath, (err) => {
                    if (err) {
                        console.error('Error while deleting image:', err);
                    } else {
                        console.log('Image deleted successfully:', imagePath);
                    }
                });
            }
    
            // Удаляем запись из базы данных
            await db.none('DELETE FROM blog WHERE post_id = $1', [post_id]);
            console.log('Post deleted successfully');
        } catch (error) {
            console.error('Error occurred while deleting post:', error);
            throw new Error('Failed to delete post');
        }
    };
    
    return { createPost, getAllPosts, getPostById, updatePost, deletePost };
};

module.exports = fp((fastify, options, next) => {
    fastify.decorate('blogDAO', BlogDAO(fastify.db));
    next();
});
