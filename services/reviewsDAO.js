const fp = require('fastify-plugin');

const ReviewsDAO = (db) => {
    const createReview = async (user_id, review_text, review_date, position_name, company_name, rating) => {
        try {
            const { review_id } = await db.one(
                'INSERT INTO reviews (user_id, review_text, review_date, position_name, company_name, rating) VALUES ($1, $2, $3, $4, $5, $6) RETURNING review_id',
                [user_id, review_text, review_date, position_name, company_name, rating]
            );
            return { review_id, user_id, review_text, review_date, position_name, company_name, rating };
        } catch (error) {
            throw new Error('Failed to create review');
        }
    };

    const getAllReviews = async () => {
        try {
            const reviews = await db.any('SELECT review_id, user_id, review_text, review_date, position_name, company_name, rating FROM reviews');
            return reviews;
        } catch (error) {
            throw new Error('Failed to fetch reviews');
        }
    };

    const getReviewById = async (review_id) => {
        try {
            const review = await db.oneOrNone('SELECT review_id, user_id, review_text, review_date, position_name, company_name, rating FROM reviews WHERE review_id = $1', [review_id]);
            if (!review) {
                throw new Error('Review not found');
            }
            return review;
        } catch (error) {
            throw new Error('Failed to fetch review');
        }
    };

    const updateReview = async (review_id, user_id, review_text, review_date, position_name, company_name, rating) => {
        try {
            await db.none(
                'UPDATE reviews SET user_id = $1, review_text = $2, review_date = $3, position_name = $4, company_name = $5, rating = $6 WHERE review_id = $7',
                [user_id, review_text, review_date, position_name, company_name, rating, review_id]
            );
            return { review_id, user_id, review_text, review_date, position_name, company_name, rating };
        } catch (error) {
            throw new Error('Failed to update review');
        }
    };

    const deleteReview = async (review_id) => {
        try {
            await db.none('DELETE FROM reviews WHERE review_id = $1', [review_id]);
        } catch (error) {
            throw new Error('Failed to delete review');
        }
    };

    return { createReview, getAllReviews, getReviewById, updateReview, deleteReview };
};

module.exports = fp((fastify, options, next) => {
    fastify.decorate('reviewsDAO', ReviewsDAO(fastify.db));
    next();
});
